﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace AspNetCoreWebApi.Model
{
    public class Book //actucal data model for the Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public double Price { get; set; } //550.200 
        public string Author { get; set; }
        public string ISBN { get; set; }
        public string Description { get; set; }
        public double VolumeNo { get; set; }
        public string BookUrl { get; set; }

        [Column(TypeName = "Date")] //2013-11-10 HH:MM:SS
        public DateTime ReleaseDate { get; set; }
        public Publisher Publisher { get; set; } //Navigation property

        //set the FK
        public int PublisherId { get; set; }
    }
}
