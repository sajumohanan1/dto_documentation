﻿using AspNetCoreWebApi.DataTransferObjects.BookDTO;
using AspNetCoreWebApi.Model;
using AutoMapper;

namespace AspNetCoreWebApi.Profiles
{
    public class BookProfile : Profile
    {
        public BookProfile() {
            //mapping data from Book to DTOReadBook
            CreateMap <Book, DTOReadBook>();
            CreateMap <DTOCreateBook, Book>();
        }     


    }
}
