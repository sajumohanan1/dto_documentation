﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AspNetCoreWebApi;
using AspNetCoreWebApi.Model;
using AutoMapper;
using AspNetCoreWebApi.DataTransferObjects.BookDTO;

namespace AspNetCoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("Application/json")] //for documenting the media type
    [Consumes("Application/json")]
    public class BooksController : ControllerBase
    {
        private readonly BooksDbContext _context;
        private readonly IMapper _mapper; //underScore- mapper is a local variable

        public BooksController(BooksDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;   
        }

        /// <summary>
        /// Get all the books
        /// </summary>
        /// <returns>Return the list of books</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
                // GET: api/Books
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOReadBook>>> GetBooks()
        {
           var bookModel =  await _context.Books.ToListAsync();
            var bookDto = _mapper.Map<List<DTOReadBook>>(bookModel);
            return bookDto;
        }

        // GET: api/Books/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOReadBook>> GetBook(int id)
        {
            var bookModel = await _context.Books.FindAsync(id);

            if (bookModel == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTOReadBook>(bookModel);
        }

        // PUT: api/Books/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBook(int id, Book book)
        {
            if (id != book.Id)
            {
                return BadRequest();
            }

            _context.Entry(book).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Books
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DTOReadBook>> PostBook(DTOCreateBook bookDto)
        {
            var bookModel = _mapper.Map<Book>(bookDto);
            _context.Books.Add(bookModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBook", new { id = bookModel.Id }, bookDto);
        }

        // DELETE: api/Books/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBook(int id)
        {
            var book = await _context.Books.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }

            _context.Books.Remove(book);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BookExists(int id)
        {
            return _context.Books.Any(e => e.Id == id);
        }
    }
}
