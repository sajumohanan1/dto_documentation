﻿using AspNetCoreWebApi.Model;
using System.Collections.Generic;
using System;

namespace AspNetCoreWebApi.Data
{
    public class DataSeed
    {
        //seed data (dummy data) for the books, publisher

        public static List<Book> GetBooks()
        {
            List<Book> books = new List<Book>()
 {
 new Book()
 {
 Id=1,
Title="Fundamentals of C# Programming",
Price=1500,
Author="Kristopher",
ISBN="IBN098781",
 Description="Learn the basis of C#",
 VolumeNo=1.1,
 BookUrl="http://sample.com",
ReleaseDate=new DateTime(2022,10,10),
 PublisherId=1
 },
 new Book()
 {
 Id=2,
Title="Advanced Programming",
Price=2500,
Author="Prof. Juliet",
ISBN="IBN088713",
Description="Advanced C++ programming",
 VolumeNo=2.1,
BookUrl="http://sample.com",
ReleaseDate=new DateTime(2021,12,11),
 PublisherId=2
 },
 new Book()
 {
 Id=3,
Title="Machine Learning",
Price=6000,
Author="Prof. Daniel",
ISBN="IBN088790",
Description="AI and Machine learning Concepts",
 VolumeNo=3.1,
BookUrl="http://sample.com",
ReleaseDate=new DateTime(2019,12,11),
 PublisherId=1
 }
 };
            return books;
        }
        public static List<Publisher> GetPublishers()
        {
            List<Publisher> publishers = new List<Publisher>();
            publishers.Add(new Publisher()
            {
                Id = 1,
                Name = "Oxford Printing",
                Service = "Publishing",
                City = "London"
            });
            publishers.Add(new Publisher()
            {
                Id = 2,
                Name = "Tech Publisher",
                Service = "Online Publishing",
                City = "Oslo"
            });
            return publishers;
        }


    }
}
