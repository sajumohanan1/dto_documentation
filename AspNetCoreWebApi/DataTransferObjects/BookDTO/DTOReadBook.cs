﻿namespace AspNetCoreWebApi.DataTransferObjects.BookDTO
{
    public class DTOReadBook //bring data from Book table
    {
        public string Title { get; set; }
        public double Price { get; set; } //550.200 
        public string Author { get; set; }
        public string ISBN { get; set; }
        public string Description { get; set; }
        public double VolumeNo { get; set; }

           }
}
