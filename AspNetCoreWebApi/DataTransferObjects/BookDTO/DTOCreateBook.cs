﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace AspNetCoreWebApi.DataTransferObjects.BookDTO
{
    public class DTOCreateBook //create a new book
    {
        public string Title { get; set; }
        public double Price { get; set; } //550.200 
        public string Author { get; set; }
        public string ISBN { get; set; }
        public string Description { get; set; }
        public double VolumeNo { get; set; }
        
        public int PublisherId { get; set; }
    }
}
